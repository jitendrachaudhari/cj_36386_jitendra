package test;
//Top Level class
class Outer{
	private int num1 = 10;	//OK
	//Non static nested class / Inner class
	class Inner{
		private int num1 = 20;	//OK  -- Access same name variable field to Inner class
		public void print( ) {
			int num1 = 30;	//OK
			System.out.println("Num1	:	"+Outer.this.num1);	//10
			System.out.println("Num1	:	"+this.num1);	//20
			System.out.println("Num1	:	"+num1);	//30
			
		}
	}	
}
public class Program {
	public static void main(String[] args) {
		Outer.Inner in = new Outer().new Inner();
		in.print( );
	}
}