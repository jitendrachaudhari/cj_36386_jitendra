package test;
class Date implements Cloneable{
	private int day;
	private int month;
	private int year;
	public Date() {
		this(0,0,0);
	}
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Date clone( ) throws CloneNotSupportedException {
		Date other = (Date) super.clone();//Creating new instance from exisiting instance
		return other;
	}
	@Override
	public String toString() {
		return "Date [day=" + day + ", month=" + month + ", year=" + year + "]";
	}
}
public class Program {	
	public static void main(String[] args) {
		try {
			Date dt1 = new Date( 6,11,2020);
			Date dt2 = dt1.clone();	
			if( dt1 == dt2 )
				System.out.println("Equal");
			else
				System.out.println("Not Equal");
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
}
