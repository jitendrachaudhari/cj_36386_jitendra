package test;
//Top Level class
class Outer{	
	private int num1 = 10;
	private static int num2 = 20;
	
	static class Inner{
		private int num3 = 30;	//OK
		private static int num4 = 40;	//OK
	}
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+Outer.num2);
		
		//System.out.println("Num3	:	"+num3);	//Not OK
		//System.out.println("Num4	:	"+num4);	//Not OK
		
		Inner in = new Inner();	//OK
		
		System.out.println("Num3	:	"+in.num3);	//OK
		System.out.println("Num4	:	"+Inner.num4);	//OK
	}
}
public class Program {
	public static void main(String[] args) {	
		Outer out = new Outer();
		out.print();
	}
}

            