package test;
//ISI
interface A{
	int number = 10;
	//public static final int number = 10;
	
	void print( );
	//public abstract void print( );
}

//Service Providers
/*abstract class B implements A{	//Interface implementation inheritance
	
}*/

//Service Providers
class B implements A{	//Interface implementation inheritance
	@Override
	public void print() {
		System.out.println("Number	:	"+A.number);
	}
}

//Service Consumer
public class Program {	
	public static void main(String[] args) {
		A a = new B();
		a.print();
	}
	public static void main1(String[] args) {
		B b  = new B();
		b.print();
	}
}
