package test;
public class Program {	//Program.class
	public static void main(String[] args) {
		// --> Method Local Inner Class
		class Complex{	//Program$1Complex.class
			private int real;
			private int imag;
			public Complex() {
				this(0,0);
			}
			public Complex(int real, int imag) {
				this.real = real;
				this.imag = imag;
			}
			@Override
			public String toString() {
				return "Complex [real=" + real + ", imag=" + imag + "]";
			}	
		}
		
		//Complex c1 = new Complex();
		Complex c1 = new Complex(10,20);
		System.out.println(c1.toString());
	}
}