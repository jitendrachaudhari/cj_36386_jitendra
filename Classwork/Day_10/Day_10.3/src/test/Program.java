package test;
//Top Level class
class Outer{
	private int num1 = 10;	//OK
	private static int num2 = 20;	//OK
	
	//Non static nested class / Inner class
	class Inner{
		private int num3 = 30;	//OK
		private static final int num4 = 40;	//OK
	}
	
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+Outer.num2);
		
		//System.out.println("Num3	:	"+num3); //NOT OK
		//System.out.println("Num4	:	"+num4); //NOT OK
		
		Inner in = new Inner();
		System.out.println("Num3	:	"+in.num3);
		System.out.println("Num4	:	"+Inner.num4);
	}
}
public class Program {
	public static void main(String[] args) {
		Outer out = new Outer();
		out.print();
	}
}