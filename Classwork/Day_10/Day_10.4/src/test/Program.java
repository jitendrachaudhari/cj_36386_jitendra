package test;
//Top Level class
class Outer{
	private int num1 = 10;	//OK
	private static int num2 = 20;	//OK
	
	//Non static nested class / Inner class  -- Access Outer class member to Inner class
	class Inner{
		private int num3 = 30;	//OK
		private static final int num4 = 40;	//OK
		
		public void print( ) {
			System.out.println("Num1	:	"+num1);	//OK
			System.out.println("Num2	:	"+num2);	//Ok
			
			System.out.println("Num3	:	"+this.num3);
			System.out.println("Num4	:	"+Inner.num4);
		}
	}	
}
public class Program {
	public static void main(String[] args) {
		Outer.Inner in = new Outer().new Inner();
		in.print( );
	}
}
