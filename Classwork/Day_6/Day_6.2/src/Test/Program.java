package Test;

import java.util.Scanner;

enum Color{
	RED, GREEN, BLUE
}
public class Program {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.print("Name	:	");
		String str = sc.nextLine();
		
		Color color = Color.valueOf(str.toUpperCase());
		String name = color.name();
		int ordinal = color.ordinal();
		
		System.out.printf("%-10s%-5d\n", name, ordinal);
	}
	public static void main5(String[] args) {
		Color[] colors = Color.values();
		for (Color color : colors) {
			String name = color.name();
			int ordinal = color.ordinal();
			System.out.printf("%-10s%-5d\n", name, ordinal);	
		}
	}
	public static void main4(String[] args) {
		//Color color = Color.RED;
		//Color color = Color.GREEN;
		Color color = Color.BLUE;
		String name = color.name();
		int ordinal = color.ordinal();
		System.out.printf("%-10s%-5d\n", name, ordinal);
	}
	public static void main3(String[] args) {
		String name = Color.BLUE.name();
		int ordinal = Color.BLUE.ordinal();
		System.out.printf("%-10s%-5d\n", name, ordinal);
	}
	public static void main2(String[] args) {
		String name = Color.GREEN.name();
		int ordinal = Color.GREEN.ordinal();
		System.out.printf("%-10s%-5d\n", name, ordinal);
	}
	public static void main1(String[] args) {
		String name = Color.RED.name();
		int ordinal = Color.RED.ordinal();
		System.out.printf("%-10s%-5d\n", name, ordinal);
	}
}