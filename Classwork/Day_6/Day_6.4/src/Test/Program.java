package Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;

public class Program {

	public static void main1(String[] args) {
		Calendar c = Calendar.getInstance();
		int day = c.get( Calendar.DATE);
		int month = c.get( Calendar.MONTH) + 1;
		int year = c.get( Calendar.YEAR);

		System.out.println(day+" / "+month+" / "+year);
		}
		public static void main2(String[] args) {
		Calendar c = Calendar.getInstance();
		int day = c.get( Calendar.DATE);
		int month = c.get( Calendar.MONTH) + 1;
		int year = c.get( Calendar.YEAR);

		int hour = c.get( Calendar.HOUR);
		int minute = c.get( Calendar.MINUTE);
		int second = c.get( Calendar.SECOND);
		int milliSecond = c.get( Calendar.MILLISECOND);

		System.out.println(hour+":"+minute+":"+second+":"+milliSecond+ " "+day+" / "+month+" / "+year);
		}
		public static void main3(String[] args) {
		LocalDate ld = LocalDate.now();
		int day = ld.getDayOfMonth();
		int month = ld.getMonthValue();
		int year = ld.getYear();
		System.out.println(day+" / "+month+" / "+year);
		} 
		public static void main5(String[] args) {
		LocalTime lt = LocalTime.now();
		int hour = lt.getHour();
		int minute = lt.getMinute();
		int second = lt.getSecond();

		System.out.println(hour+":"+minute+":"+second);
		}
		public static void main(String[] args) {
		Date date = new Date();
		int day = date.getDate();
		int month = date.getMonth() + 1;
		int year = date.getYear() + 1900;
		System.out.println(day+" / "+month+" / "+year);
		} 
}
