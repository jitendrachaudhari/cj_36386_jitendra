package test;
enum Day{
	SUN(1), MON("MonDay"), TUES(3,"TuesDay");
	private int dayNumber;
	private String dayName;
	private Day(int dayNumber) {
		this.dayNumber = dayNumber;
	}
	public int getDayNumber() {
		return dayNumber;
	}
	public String getDayName() {
		return dayName;
	}
	private Day(int dayNumber, String dayName) {
		this.dayNumber = dayNumber;
		this.dayName = dayName;
	}
	private Day(String dayName) {
		this.dayName = dayName;
	}
	@Override
	public String toString() {
		return String.format("%-10s%-5d",this.dayName, this.dayNumber);
	}
}
public class Program {
	public static void main(String[] args) {
		Day[] days = Day.values();
		for (Day day : days) {
			String name = day.name();
			int ordinal = day.ordinal();
			System.out.println(name+"	"+ordinal+"	"+day.toString());
		}
	}
	public static void main1(String[] args) {
		Day[] days = Day.values();
		for (Day day : days) {
			String name = day.name();
			int ordinal = day.ordinal();
			String dayName = day.getDayName();
			int dayNumber = day.getDayNumber();
			System.out.println(name+"	"+ordinal+"	"+dayName+"	"+dayNumber);
		}
	}
}