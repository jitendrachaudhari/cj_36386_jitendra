package Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class Program{
	public static void main(String[] args) {
		Integer n = new Integer(0);
		Class<?> c = n.getClass();
		Program.printClassInfo(c);
		Program.printFieldInfo(c);
		Program.printMethodInfo(c);
		Program.printCtorInfo(c);
	}

	private static void printCtorInfo(Class<?> c) {
		Constructor<?>[] ct= c.getConstructors();
		for(Constructor<?> ctr : ct ) {
			System.out.println(ctr);
		}

	}


	private static void printMethodInfo(Class<?> c) {
		Method[] ms= c.getMethods();
		for(Method m : ms ) {
			//System.out.println(m.getName());
		}

	}

	private static void printFieldInfo(Class<?> c) {
		Field[] fs = c.getFields();
		for(Field f : fs) {
			// System.out.println(f);
		}
	}

	private static void printClassInfo(Class<?> c) {
		String s = c.getSimpleName();

		System.out.println(s);
		Package[] p = c.getPackage().getPackages();
		for(Package pack : p) {

			//System.out.println(pack);
		}

		int n = c.getModifiers();
		String s1= Modifier.toString(n);
		System.out.println(s1);
		String st= Modifier.toString(c.getModifiers());
		//System.out.println(st);
	}


} 