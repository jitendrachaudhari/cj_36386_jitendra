package Test;

import java.lang.reflect.Field;

class Point {
	private int xpos;
	private int ypos;
	public Point(int xpos, int ypos) {
		this.xpos = xpos;
		this.ypos = ypos;
	}
	@Override
	public String toString() {
		return "Point [xpos=" + xpos + ", ypos=" + ypos + "]";
	}

}
public class Program {

	public static void main(String[] args) {

		try {
			Point pt1 = new Point(10, 20);
			
			System.out.println(pt1.toString());
			
			Class <?> p = pt1.getClass();
			System.out.println(p.toString());
			
			Field f = p.getDeclaredField("xpos");
			f.setAccessible(true);
			f.setInt(pt1, 50);
			
			Field f1 = p.getDeclaredField("ypos");
			f1.setAccessible(true);
			f1.setInt(pt1, 80);
			
			System.out.println(pt1.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
