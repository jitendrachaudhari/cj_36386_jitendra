package Test;

@FunctionalInterface
interface A{	
	void f1( );	

	@FunctionalInterface
	interface B{
		void f1( );
		static void f2( ) {
		}
		static void f3( ) {
		}
		default void f4( ) {
		}
		default void f5( ) {
		}
	}
	@FunctionalInterface
	interface B{	//Error
		void f1( );	
		void f2( );	
	}
	public class Program {
		public static void main(String[] args) {

		}
	}
}