package Test;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention( value = RetentionPolicy.RUNTIME)
@Target( value = ElementType.TYPE )
@Repeatable( value = Authors.class )
@interface Author{
	String name( );	
	String date( );
}
@Retention( value = RetentionPolicy.RUNTIME)
@Target( value = ElementType.TYPE )
@interface Authors{
	Author[] value();
}

@Author(name="ABC", date="09/11/2020")
@Author(name="XYZ", date="09/11/2020")		
class Book{
	}
public class Program {
	public static void main(String[] args) {
		Class<?> c = Book.class;
		Annotation[] annotations = c.getDeclaredAnnotations();
		for (Annotation annotation : annotations) {
			if( annotation instanceof Authors ) {
				Authors authors = (Authors) annotation;
				for (Author author : authors.value()) {
					System.out.println(author.name()+"	"+author.date());
				}
			}
		}
	}
}