package test;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Inherited
@Retention( value = RetentionPolicy.RUNTIME)
@Target( value = ElementType.TYPE )
@interface Author{
	String name( );
}

@Author(name="ABC")
class Book{
	
}
public class Program {
	public static void main(String[] args) {
		Class<?> c = Book.class;
		Annotation[] annotations = c.getDeclaredAnnotations();
		for (Annotation annotation : annotations) {
			if( annotation instanceof Author ) {
				Author author = (Author) annotation;
				System.out.println(author.name());
			}
		}
	}
}