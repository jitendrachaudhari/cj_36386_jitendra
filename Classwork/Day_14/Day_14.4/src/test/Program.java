package test;
class Task implements Runnable{
	@Override
	public void run() {
		System.out.println(Thread.currentThread().getName()+"	"+Thread.currentThread().getState());
		System.out.println("Inside Business Logic Method");
	}
}
public class Program {
	public static void main(String[] args) {
		Runnable target = new Task( );
		
		Thread th1 = new Thread(target); 
		th1.setName("UserThread-0");
		th1.start();	
		
		Thread th2 = new Thread(target); 
		th2.setName("UserThread-1");
		th2.start();	 
		
		Thread th3 = new Thread(target); 
		th3.setName("UserThread-2");
		th3.start();
		
	}
	public static void main2(String[] args) {
		Runnable target = new Task( );	
		Thread th = new Thread(target); 
		th.setName("UserThread-0");
		th.start();	  
		//th.start();	
	}
	public static void main1(String[] args) {
		Thread th = new Thread( );
		System.out.println(th.getState());
	}
}