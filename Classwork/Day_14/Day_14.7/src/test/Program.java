package test;
class CThread extends Thread{
	public CThread( String name ) {
		super( name );
		this.start();
	}
	@Override
	public void run() {
		System.out.println("Inside Business Logic Method");
	}
}
public class Program {
	public static void main(String[] args) {
		Thread th1 = new CThread( "A" );
		Thread th2 = new CThread( "B" );
		Thread th3 = new CThread( "C" );
	}
}