
package test;
class CThread implements Runnable{
	private Thread thread;
	public CThread( String name ) {
		this.thread = new Thread(this, name);
		this.thread.start();
		System.out.println(this.thread.getName());

	}
	@Override
	public void run() {
		System.out.println("Inside Business Logic Method");
	}
}
public class Program {
	public static void main(String[] args) {
		CThread th1 = new CThread("A");
		CThread th2 = new CThread("B");
		CThread th3 = new CThread("C");
	}
}