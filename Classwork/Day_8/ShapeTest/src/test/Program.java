package test;
import org.sunbeam.dac.lib.ShapeFactory;
import org.sunbeam.dac.lib.ShapeType;
public class Program {
	public static void main(String[] args) {
		ShapeType choice;
		ShapeTest test = new ShapeTest();
		while ((choice = ShapeTest.menuList()) != ShapeType.EXIT) {
			test.setShape(ShapeFactory.getInstance(choice));
			test.acceptRecord();
			test.printRecord();
		}
	}
}
