package test;

class A{
	public void print(int number) {
		System.out.println("A.print()");
	}
}
class B extends A{
	@Override
	public void print(int number) {
		System.out.println("B.print()");
	}
}
public class Program {

	public static void main(String[] args) {
		A a = new B();
		a.print(10);
		
		B b = new B();
		b.print(20);
	}

}
