package test;
class Test{
	static {
		System.out.println("Static 1");
		Test.num1 = 10;
		Test.num2 = 20;
	}
	static {
		System.out.println("Static 3");
		Test.num3 = 30;
	}
	private static int num1;
	private static int num2;
	private static int num3;
	
	public static void print( ) {
		System.out.println("Num1	:	"+Test.num1);
		System.out.println("Num2	:	"+Test.num2);
		System.out.println("Num3	:	"+Test.num3);
	}
	
}
public class Program {
	public static void main(String[] args) {
		Test.print();
	}
}