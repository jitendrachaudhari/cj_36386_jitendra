package test;

class Complex{

	private int real;
	private int imag;
	public Complex(int real, int imag) {
		this.real = real;
		this.imag = imag;
	}
	public int getReal() {
		return real;
	}
	public void setReal(int real) {
		this.real = real;
	}
	public int getImag() {
		return imag;
	}
	public void setImag(int imag) {
		this.imag = imag;
	}
	@Override
	public String toString() {
		return "Complex [real=" + real + ", imag=" + imag + "]";
	}
}
public class Program {
	public static void main(String[] args) {
		final Complex c1 = new Complex( 111, 2222 );
		c1.setReal(333);
		c1.setImag(444);
		System.out.println(c1.toString());
	}
	public static void main2(String[] args) {
		Complex c1 = new Complex( 111, 2222 );
		System.out.println(c1.toString());	
		
		c1 = new Complex( 50, 60 );
		System.out.println(c1.toString());	
	}
}
	