package test;

class Test{
	public static final int num1 = 111;
	public static final int num2 = 222;
	public Test() {

	}
	public void print( ) {
		System.out.println("Num1	:	"+this.num1);
		System.out.println("Num2	:	"+this.num2);
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test( );
		t.print();
	}
}
	