package test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptArray(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < arr.length; ++ row ) {
				for( int col = 0; col < arr[ row ].length; ++ col ) {
					System.out.print("arr[ "+row+" ][ "+col+" ]	:	");
					arr[ row ][ col ] = sc.nextInt();
				}
			}
		}
	}
	private static void printArray(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < arr.length; ++ row ) {
				for( int col = 0; col < arr[ row ].length; ++ col ) {
					System.out.print(arr[ row ][ col ]+"	");
				}
				System.out.println();
			}
		}
	}
	public static void main(String[] args) {
		int[][] arr = new int[ 4 ][ ];
		arr[ 0 ] = new int[  ] {1,2,3};
		arr[ 1 ] = new int[ ] {4,5,6,7};
		arr[ 2 ] = new int[  ] {8,9};
		arr[ 3 ] = new int[  ] {10,11,12,13,14};
		for( int row = 0; row <arr.length; ++ row )
			System.out.println(Arrays.toString(arr[ row]));
	}
	public static void main4(String[] args) {
		int[][] arr = new int[ 4 ][ ];
		arr[ 0 ] = new int[  ] {111,222,333};
		arr[ 1 ] = new int[ ] {444,555,666,777};
		arr[ 2 ] = new int[  ] {888,999};
		arr[ 3 ] = new int[  ] {1000,1010,1212,1313,1415};
		Program.printArray(arr);
	}
	public static void main2(String[] args) {
		int[][] arr = new int[ 4 ][ ];
		arr[ 0 ] = new int[ 1 ];
		arr[ 1 ] = new int[ 2 ];
		arr[ 2 ] = new int[ 3 ];
		arr[ 3 ] = new int[ 4 ];
		Program.acceptArray( arr );
		Program.printArray(arr);
	}
	public static void main1(String[] args) {		
		int[][] arr = new int[ 4 ][ ];
		arr[ 0 ] = new int[ 1 ];
		arr[ 1 ] = new int[ 2 ];
		arr[ 2 ] = new int[ 3 ];
		arr[ 3 ] = new int[ 4 ];
		
	}
}
	