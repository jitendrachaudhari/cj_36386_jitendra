package test;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptArray(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < arr.length; ++ row ) {
				for( int col = 0; col < arr[ row ].length; ++ col ) {
					System.out.print("arr[ "+row+" ][ "+col+" ]	:	");
					arr[ row ][ col ] = sc.nextInt();
				}
			}
		}
	}
	private static void printArray(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < arr.length; ++ row ) {
				for( int col = 0; col < arr[ row ].length; ++ col ) {
					System.out.print(arr[ row ][ col ]+"	");
				}
				System.out.println();
			}
		}
	}
	public static void main4(String[] args) {
		int[][] arr = new int[  ][  ] {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		for( int row = 0; row <arr.length; ++ row )
			System.out.println(Arrays.toString(arr[ row]));
	}
	public static void main3(String[] args) {
		
		int[][] arr = {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		Program.printArray(arr);
	}
	public static void main(String[] args) {
		int[][] arr = new int[ 4 ][ 3 ];
		Program.acceptArray( arr );
		Program.printArray(arr);
	}
	}

	