package test;
class Node{
	int data;
	Node next;//null
	public Node( int data ) {
		this.data = data;
	}
}
class LinkedList{
	Node head;	//null
	Node tail;	//null
	public boolean empty( ) {
		return this.head == null;
	}
	public void addLast( int element ) {
		Node newNode = new Node(element);
		if( this.empty())
			this.head = newNode;
		else
			this.tail.next = newNode;
		this.tail = newNode;
	}
	public void printList( ) {
		Node trav = this.head;
		while( trav != null ) {
			System.out.print(trav.data+"	");
			trav = trav.next;
		}
		System.out.println();
	}
}
public class Program {
	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		list.addLast( 10 );
		list.addLast( 20 );
		list.addLast( 30 );
		
		list.printList( );
	}
}

            