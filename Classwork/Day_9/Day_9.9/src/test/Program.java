                
package test;
class Employee{
	private String name;
	private int empid;
	private float salary;
	public Employee(String name, int empid, float salary) {
		this.name = name;
		this.empid = empid;
		this.salary = salary;
	}
	//Employee this  = emp1;
	//Object obj = emp2; //Upcasting
	@Override
	public boolean equals(Object obj) {
		if( obj != null ) {
			Employee other = (Employee) obj;
			if( this.empid == other.empid )
				return true;
		}
		return false;
	}
}
public class Program {
	public static void main(String[] args) {
		Employee emp1 = new Employee("Sandeep", 33, 45000);
		Employee emp2 = new Employee("Sandeep", 33, 45000);
		//Employee emp2 = new Employee("Sandeep", 34, 45000);
		//Employee emp2 = null;
		if( emp1.equals(emp2) )	//OK : Compares state of instances
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
		//Output : Equal
	}
}
