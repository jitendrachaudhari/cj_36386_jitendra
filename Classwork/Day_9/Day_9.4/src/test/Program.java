package test;

import java.util.ArrayList;
import java.util.Date;

public class Program {
	private static ArrayList< Integer > getIntegerList( ){
		ArrayList<Integer> list = new ArrayList<>();
		list.add( 10 );
		list.add( 20 );
		list.add( 30 );
		return list;
	}
	private static ArrayList< Double > getDoubleList( ){
		ArrayList<Double> list = new ArrayList<>();
		list.add( 10.1 );
		list.add( 20.2 );
		list.add( 30.3 );
		return list;
	}
	private static ArrayList< String > getStringList( ){
		ArrayList<String> list = new ArrayList<>();
		list.add( "DAC" );
		list.add( "DMC" );
		list.add( "DESD" );
		return list;
	}
	private static void printList(ArrayList<? super Integer> list) {
		if( list != null ) {
			for( Object element : list )
				System.out.println(element);
		}
	}
	public static void main3(String[] args) {
		ArrayList<Double> doubleList = Program.getDoubleList();
		//Program.printList(doubleList);//Not OK
	}
	public static void main(String[] args) {
		ArrayList<Object> list = new ArrayList<Object>();
		Object o1 = new String("DAC");
		list.add(o1);
		
		Object o2 = new Integer(120);
		list.add(o2);
		
		Object o3 = new Date();
		list.add(o3);
		
		Program.printList(list);
	}
	public static void main2(String[] args) {
		ArrayList<Number> list = new ArrayList<Number>( );
		Number n1 = new Integer(10);
		list.add(n1);
		Number n2 = new Double(20.2);
		list.add(n2);
		
		Program.printList(list);
	}
	public static void main1(String[] args) {
		ArrayList<Integer> intList = Program.getIntegerList();
		Program.printList( intList );	//OK
		
		ArrayList<Double> doubleList = Program.getDoubleList();
		//Program.printList( doubleList );	//NOT OK
		
		ArrayList<String> stringList = Program.getStringList( );
		//Program.printList( stringList );	//NOT OK
	}
}

            