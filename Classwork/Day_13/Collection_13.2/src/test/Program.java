package test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

public class Program {
	private static Map<Integer, String> getMap() {
		Map<Integer, String> map = new Hashtable<>(); //Upcasting
		map.put(7, "DAC");
		map.put(12, "DMC");
		map.put(35, "DESD");
		map.put(42, "DBDA");
		return map;
	}
	private static void find(Map<Integer, String> map, int id) {
		if( map != null ) {
			Integer key = new Integer(id);
			if( map.containsKey(key)) {
				String value = map.get(key);
				System.out.println(key+" : "+value);
			}
			else
				System.out.println(key+" not found.");
		}
	}
	private static void remove(Map<Integer, String> map, int id) {
		if( map != null ) {
			Integer key = new Integer(id);
			if( map.containsKey(key)){
				String value = map.remove(key);
				System.out.println(key+" : "+value+" is removed.");
			}
			else
				System.out.println(key+" not found.");
		}
	}
	public static void printKeys( Map<Integer, String> map ) {
		if( map != null ) {
			Set<Integer> keys = map.keySet();
			for (Integer key : keys) {
				System.out.println(key);
			}
		}
	}
	public static void printValues( Map<Integer, String> map ) {
		if( map != null ) {
			Collection<String> values = map.values();
			for (String value : values) {
				System.out.println(value);
			}
		}
	}
	public static void print( Map<Integer, String> map ) {
		if( map != null ) {
			Set<Entry<Integer, String>> entries = map.entrySet();
			for (Entry<Integer, String> entry : entries) {
				System.out.println(entry.getKey()+"	"+entry.getValue());
			}
		}
	}
	public static void main(String[] args) {
		Map<Integer, String> map = Program.getMap( );
		//Program.printKeys( map );
		//Program.printValues(map);
		//Program.print( map );
		//Program.find( map, 12 );
		//Program.remove( map, 12 );
		
		List<Integer> keys = new ArrayList<Integer>( map.keySet());
		keys.forEach(System.out::println);
		
		List<String> values = new Vector<String>(map.values());
		values.forEach(System.out::println);
	}
}
