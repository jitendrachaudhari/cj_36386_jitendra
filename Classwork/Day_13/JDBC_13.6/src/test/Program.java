package test;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import utils.DBUtils;

public class Program {
	public static void main(String[] args) {
		try ( Connection connection = DBUtils.getConnection();
			  Statement statement = connection.createStatement();){
			String sql = "DELETE FROM books WHERE book_id=1026";
			int count = statement.executeUpdate(sql);
			System.out.println(count+" row(s) affected");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main2(String[] args) {
		try ( Connection connection = DBUtils.getConnection();
			  Statement statement = connection.createStatement();){
			String sql = "UPDATE books SET price = 5000 WHERE book_id=1026";
			int count = statement.executeUpdate(sql);
			System.out.println(count+" row(s) affected");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main1(String[] args) {
		try ( Connection connection = DBUtils.getConnection();
			  Statement statement = connection.createStatement();){
			String sql = "INSERT INTO books VALUES(1026,'OS','ABC','PQR',450)";
			int count = statement.executeUpdate(sql);
			System.out.println(count+" row(s) affected");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
