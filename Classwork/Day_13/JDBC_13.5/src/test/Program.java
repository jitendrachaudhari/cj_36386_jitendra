package test;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import utils.DBUtils;

public class Program {
	public static void main(String[] args) {
		try ( Connection connection = DBUtils.getConnection();
			  Statement statement = connection.createStatement();){
			String sql = "SELECT * FROM books";
			ResultSet rs = statement.executeQuery(sql);

			while (rs.next()) {
				int bookId = rs.getInt("book_id");
				String subjectName = rs.getString("subject_name");
				String bookName = rs.getString("book_name");
				String authorName = rs.getString("author_name");
				float price = rs.getFloat("price");
				System.out.printf("%-5d%-10s%-55s%-40s%-10.2f\n", bookId, subjectName, bookName, authorName, price);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
