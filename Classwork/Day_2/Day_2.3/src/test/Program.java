package test;

public class Program {

	public static void main(String[] args) {
		double num1 = 10.5;
		int num2 = (int) num1;	//--------------------->>Narrowing
		System.out.println("Num2	:	"+num2);
	}
	public static void main2(String[] args) {
		int num1 = 10;
		//double num2 = (double)num1;	//----------------->>Widening -----> Type cast optional
		double num2 = num1;	
		System.out.println("Num2	:	"+num2);
	}
	public static void main1(String[] args) {
		int num1 = 10;	
		int num2 = num1;
		System.out.println("Num2	:	"+num2);
	}
}
