package test;

class Employee{
}
public class Program {
	private static int GetHashCode(int number) {
		int prime = 31;
		int result = 1;
		result = result * number + prime * number; 
		return result;
	}
	public static void main1(String[] args) {
		int number = 127;
		int hashCode = Program.GetHashCode( number );
		System.out.println(number+"	"+hashCode);
		System.out.println(number+"	"+Integer.toHexString(hashCode));
	}
	public static void main(String[] args) {
		Employee emp  = new Employee();
		int hashCode = emp.hashCode();	//2018699554
		System.out.println(Integer.toHexString(hashCode));
	}
}
