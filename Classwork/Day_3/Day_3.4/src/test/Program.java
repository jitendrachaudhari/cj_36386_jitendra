package test;

class Account{
	private int number;	
	private String type;
	private float balance;
	
	public Account() {
	}
	public Account(int number, String type, float balance) {
		this.number = number;
		this.type = type;
		this.balance = balance;
	}
	public void printRecord(  ) {
		System.out.println("Number	:	"+this.number);
		System.out.println("Type	:	"+this.type);
		System.out.println("Balance	:	"+this.balance);
	}
}

public class Program {

	public static void main(String[] args) {
			Account account = new Account();
			account.printRecord();
		}
		public static void main4(String[] args) {
			Account account = null;
			account = new Account();	
			account.printRecord(); 
			}
		public static void main3(String[] args) {
			Account account = null;	
			if( account != null )
				account.printRecord();
		}
		public static void main2(String[] args) {
			Account account = null;	
			account.printRecord(); // ----------------->>NullPointerException
		}
		public static void main1(String[] args) {
			//Account account;
			//account.printRecord( );	//Compiler Error
		}
	}

}
