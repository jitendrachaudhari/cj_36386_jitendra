package test;

import java.util.Scanner;

class Account {
	private int number;
	private String type;
	private float balance;

	public Account() {
		this(0, "", 0.0f);
	}

	public Account(int number, String type, float balance) {
		this.number = number;
		this.type = type;
		this.balance = balance;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
}

public class Program {
	static Scanner sc = new Scanner(System.in);

	public static void acceptRecord(Account account) {
		System.out.print("Number	:	");
		account.setNumber(sc.nextInt());
		System.out.print("Type	:	");
		sc.nextLine();
		account.setType(sc.nextLine());
		System.out.print("Balance	:	");
		account.setBalance(sc.nextFloat());
	}

	public static void printRecord(Account account) {
		System.out.println("Number	:	" + account.getNumber());
		System.out.println("Type	:	" + account.getType());
		System.out.println("Balance	:	" + account.getBalance());
	}

	public static int menuList() {
		System.out.println("0.Exit");
		System.out.println("1.Accept Record");
		System.out.println("2.Print Record");
		System.out.print("Enter choice	:	");
		int choice = sc.nextInt();
		return choice;
	}

	public static void main(String[] args) {
		int choice;
		Account account = new Account();
		while ((choice = Program.menuList()) != 0) {
			switch (choice) {
			case 1:
				Program.acceptRecord(account);
				break;
			case 2:
				Program.printRecord(account);
				break;
			}
		}
	}
}