package Test;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Program{
	static Scanner sc= new Scanner(System.in);
	
	
	public static void main(String[] args) {
		try(Scanner sc = new Scanner(System.in)){
			System.out.print("String	:	");
			String s1 = sc.nextLine();
			String delim = " ";
			StringTokenizer stk = new StringTokenizer(s1,delim);
			String token = null;
			while( stk.hasMoreTokens()) {
				token = stk.nextToken();
				
				StringBuilder sb = new StringBuilder(token);
				sb.reverse();
				token = sb.toString();
				System.out.println("String	:	"+token);
				
			}
			
			
		}
	}
}
