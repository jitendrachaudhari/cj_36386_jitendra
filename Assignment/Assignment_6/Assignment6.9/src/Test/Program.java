package Test;


import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {
	
	static String word;
	public static int indexOf(char ser)
	{
		
		char[] arr = word.toCharArray();
		for(int i = 0;i<arr.length;i++)
		{
			if(arr[i]==ser)
			{
				return i+1;
			}
		}
		return -1;
	}
	public static int lastIndexOf(char ser)
	{
		
		char[] arr = word.toCharArray();
		for(int i = arr.length-1;i>0;i--)
		{
			if(arr[i]==ser)
			{
				return i+1;
			}
		}
		return -1;
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Word:  ");
		word= sc.nextLine();
	
		
		System.out.println("Last index " + lastIndexOf('h'));
	}
}