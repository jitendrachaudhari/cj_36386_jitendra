package Test;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Program{
	static Scanner sc= new Scanner(System.in);
	static void splitToExtract(String s) {
		String str = s;
		String delim = "-";
		StringTokenizer stk = new StringTokenizer(str,delim);
		String token = null;
		while( stk.hasMoreTokens()) {
			token = stk.nextToken();
			if(token.length()==3) {
			System.out.println("Area Code : " + token);
			}else if(token.length()==4) {
				System.out.println("Phone No : " + token);
			}
		}
	}
	public static void main(String[] args) {
		System.out.println("Enter phone no in 111-1111 format");
		String s=sc.nextLine();
		Program.splitToExtract(s);
		
		
	}
}
