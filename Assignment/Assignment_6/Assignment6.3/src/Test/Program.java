package Test;


import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Program {
	
	public static String formSentence()
	{
		String[] article = new String[] {"the","a","one","some","any"};
		String[] nouns = new String[] {"boy","girl","dog","town","car"};
		String[] verbs = new String[] {"drove","jumped","ran","walked","skipped"};
		String[] prepositions = new String[] {"to","from","over","under","on"};
		String  sentence = "";
		String shortarticle = "";
		
		int min = 0;
		int max = 4;
		int[] index = new int[5];
		for(int i = 0;i<4;i++)
		{
			index[i] = (int)(Math.random() * (max+1)) ;
		}
		if(article[index[0]].length() > 1)
		{
			shortarticle = article[index[0]].substring(1);
		}
		sentence += article[index[0]].substring(0, 1).toUpperCase() + shortarticle + " " + nouns[index[1]] + " " + verbs[index[2]] + " " + prepositions[index[3]] + " "+ article[index[0]] + " " + nouns[index[1]];
		return sentence;
		
	}
	
	
	public static void main(String[] args) {
		
		for(int i =0;i<20;i++)
		{
			System.out.println(i +"  " + formSentence());
		}
	}
	
	
	
	
	
}