package Test;

public class Rectangle {
private double length;
private double width;

public Rectangle() {
length = 1;
width = 1;
}

Rectangle(double length, double width) {
this.length = length;
this.width = width;
}

public double getArea() {
return (length * width);
}

public double getPerimeter() {
return (2 * (length + width));
}

@Override
public String toString() {
return "Rectangle [length=" + length + ", width=" + width + "]";
}

} 