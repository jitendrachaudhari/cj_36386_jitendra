package Test;

public class Circle {

private double radius;
static final double PI =3.14;

public Circle ()
{
radius = 10;

}

public Circle(double radius) {

this.radius = radius;
}
public double getArea()
{
return PI*radius*radius;
}

public double getPerimeter() {
return (2*PI*radius);
}

@Override
public String toString() {
return "Circle [radius=" + radius + "]";
}



} 