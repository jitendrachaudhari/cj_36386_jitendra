package Test;

public class Triangle {

private double base;
private double height;

public Triangle() {
base = 1;
height = 1;
}

public Triangle(double base, double height) {

this.base = base;
this.height = height;
}

public double getArea() {
return ((base* height)/2);
}

public double getPerimeter() {
return ((2*height)+base);//Isosceles triangle
}

} 
