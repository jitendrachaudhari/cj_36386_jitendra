package Test;

public class Person {
	private String name;	//null
	private Date joinDate;	//null
	private Address currentAddress;	//null
	public Person() {
		this.name = new String();
		this.joinDate = new Date();
		this.currentAddress = new Address();
	}
	public Person(String name, Date joinDate, Address currentAddress) {
		this.name = name;
		this.joinDate = joinDate;
		this.currentAddress = currentAddress;
	}
	public Person(String name,int day, int month, int year, String cityName, String stateName, int pinCode) {
		this.name = name;
		this.joinDate = new Date(day, month, year);
		this.currentAddress = new Address(cityName, stateName, pinCode);
	}
	public void PrintPerson( ) {
		System.out.println(this.name);
		System.out.println(this.joinDate);
		System.out.println(this.currentAddress);		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getJoinDate() {
		return joinDate;

	}
	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	public Address getCurrentAddress() {
		return currentAddress;
	}
	public void setCurrentAddress(Address currentAddress) {
		this.currentAddress = currentAddress;
	}
	public String toString() {
		return "Person [name=" + name + ", joinDate=" + joinDate + ", currentAddress=" + currentAddress + "]";
	}
	
	public static void main(String[] args) {
		Person p = new Person("Virat", 1, 10, 2019, "Pune", "MH", 1234 );

		p.PrintPerson();
	}
	
}
