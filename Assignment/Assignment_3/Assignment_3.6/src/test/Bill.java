package test;

public class Bill{
	private String plan;
	private int callCount;
	private int smsCount;

	public Bill() {
		this("", 0, 0);
	}

	public Bill(String plan, int callCount, int smsCount) {
		this.plan = plan;
		this.callCount = callCount;
		this.smsCount = smsCount;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public int getCallCount() {
		return callCount;
	}

	public void setCallCount(int callCount) {
		this.callCount = callCount;
	}

	public int getSmsCount() {
		return smsCount;
	}

	public void setSmsCount(int smsCount) {
		this.smsCount = smsCount;
	}

	public double getTotalBill()
	{
		double bill=0;
		if(this.plan=="PLAN A")
		{
			bill=199;
			bill=bill+1*this.smsCount;
			if (this.callCount <= 50)
		    {
		        bill =bill+ 0;
		    }
		    else if (this.callCount > 50 && this.callCount <= 100)
		    {
		        this.callCount = this.callCount - 50;
		        bill = bill+(0.50 *this.callCount);
		    }
		    else if(this.callCount>100)
		    {
		        this.callCount =this.callCount - 100;
		        bill = bill+(0.50 *50) + (1 *this.callCount);      
		    }	
			
		}else if(this.plan=="PLAN B")
		{
			bill=299;
			bill=bill+0.75*this.smsCount;
			if (this.callCount <= 75)
	    {
	        bill =bill+ 0;
	    }
	    else if (this.callCount > 75 && this.callCount <= 150)
	    {
	        this.callCount = this.callCount - 75;
	        bill = bill+(0.40 *this.callCount);
	    }
	    else if(this.callCount>150)
	    {
	        this.callCount =this.callCount - 150;
	        bill = bill+(0.40 *75) + (0.80 *this.callCount);     
	    }
			
			
		}else if(this.plan=="PLAN C")
		{
			bill=399;
			bill=bill+0.50*this.smsCount;
			if (this.callCount <= 100)
		    {
		        bill =bill+ 0;
		    }
		    else if (this.callCount > 100 && this.callCount <= 200)
		    {
		        this.callCount = this.callCount - 100;
		        bill = bill+(0.30 *this.callCount);
		    }
		    else if(this.callCount>200)
		    {
		        this.callCount =this.callCount - 200;
		        bill = bill+(0.30 *100) + (0.60 *this.callCount);
		       
		    }
			
		}
		
		return (bill+bill*12.5/100);
	}
	
	
}