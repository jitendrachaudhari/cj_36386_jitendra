package test;

import java.io.IOException;
import java.util.Scanner;

class Invoice {
	private String aPartNumber;
	private String aPartDescription;
	private int aQuantityOfItemPurchased;
	private double aPricePerItem;
	double amount;
	
	 public Invoice() {  }
	
	public Invoice(String aPartNumber , String aPartDescription, int aQuantityOfItemPurchased , double aPricePerItem ) {
		this.aPartNumber = aPartNumber;
		this.aPartDescription = aPartDescription;
		this.aQuantityOfItemPurchased = aQuantityOfItemPurchased;
		this.aPricePerItem = aPricePerItem;
	}

	public String getaPartNumber() {
		return aPartNumber;
	}

	public void setaPartNumber(String aPartNumber) {
		this.aPartNumber = aPartNumber;
	}

	public String getaPartDescription() {
		return aPartDescription;
	}

	public void setaPartDescription(String aPartDescription) {
		this.aPartDescription = aPartDescription;
	}

	public int getaQuantityOfItemPurchased() throws IOException {
		return aQuantityOfItemPurchased;
	}

	public void setaQuantityOfItemPurchased(int aQuantityOfItemPurchased) {
		this.aQuantityOfItemPurchased = aQuantityOfItemPurchased;
	}

	public double getaPricePerItem() throws IOException {
		return aPricePerItem;
	}

	public void setaPricePerItem(double aPricePerItem) {
		this.aPricePerItem = aPricePerItem;
	}
	
	 public double getInvoiceAmount()
	    {
	        amount = aPricePerItem * aQuantityOfItemPurchased;
	        System.out.println(" Amount "+ amount);
	
	        amount = (amount>0)?amount:0;
	        return amount;
	    }
	
	public void acceptRecord(Invoice invoice) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("aPartDescription	:	");
		invoice.setaPartDescription(sc.nextLine());
		System.out.print("aPartNumber	:	");
		invoice.setaPartNumber( sc.nextLine());
		System.out.print("aQuantityOfItemPurchased	:	");
		invoice.setaQuantityOfItemPurchased(sc.nextInt());

		System.out.print("aPricePerItem	:	");
		invoice.setaPricePerItem(sc.nextDouble());
		sc.close();
	}

	public void displayInfo() {

		System.out.println("part number\t"+ this.aPartNumber+"\tpart description\t"+this.aPartDescription+"\tnum_of_items\t"+this.aQuantityOfItemPurchased+"\tprice_of_items\t"+this.aPricePerItem);
		System.out.println("Amount :"   + this.amount);
        System.out.println();
	}
}

	

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Invoice invoice = new Invoice();
		invoice.acceptRecord(invoice);
        invoice.getInvoiceAmount();
        invoice.displayInfo();
	}

}