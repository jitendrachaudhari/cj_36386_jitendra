package test;

import java.util.Scanner;

class Date{
	
	private int date;
    private int month;
    private int year;
    
    Date()
    {
        date=28;
        month=10;
        year=2020;
    }
    
	public Date(int date, int month, int year) {
		this.date = date;
		this.month = month;
		this.year = year;
	}


	public int getDate() {
		return date;
	}


	public void setDate(int date) {
		this.date = date;
	}


	public int getMonth() {
		return month;
	}


	public void setMonth(int month) {
		this.month = month;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}
    
	public void display()
    {
        System.out.println("Entered Date : "+date+"/"+month+"/"+year);
    }
}

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       Date date = new Date();
       Scanner sc = new Scanner(System.in);
       System.out.print("Date	:	");
	   date.setDate(sc.nextInt());
	   
	   System.out.print("Month	:	");
	   date.setMonth(sc.nextInt());
	   
	   System.out.print("Year	:	");
	   date.setYear(sc.nextInt());

       date.display();
       
       sc.close();
       
	}

}