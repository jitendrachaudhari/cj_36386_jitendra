package test;

import java.util.Calendar;
import java.util.Scanner;

class HeartRates {

	private String firstName;
	private String lastName;
	private int birthYear;

	HeartRates() {
		this.firstName = "";
		this.lastName = "";
		this.birthYear = 1800;
	}

	public HeartRates(String firstName, String lastName, int birthYear) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthYear = birthYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

}

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		HeartRates myHealth = new HeartRates();

		Calendar cal = Calendar.getInstance();
		int k = cal.get(Calendar.YEAR);

		System.out.print("Person's First Name: ");
		String first = input.nextLine();

		myHealth.setFirstName(first);

		System.out.print("Person's Last Name: ");
		String last = input.nextLine();

		myHealth.setLastName(last);

		System.out.print("Person's Year of birth: ");
		int birthY = input.nextInt();

		myHealth.setBirthYear(birthY);

		System.out.printf("First Name: %s%n", first);
		System.out.printf("Last Name: %s%n", last);

		int age = k - birthY;
		System.out.printf("Age: %d%n", age);

		int maxHeartRate = 220 - age;
		System.out.printf("Maximum Heart Rate: %d%n", maxHeartRate);

		double targetMin = maxHeartRate * 0.50;
		double targetMax = maxHeartRate * 0.85;

		System.out.printf("Target heart rate range: %.0f", targetMin);
		System.out.printf(" - %.0f", targetMax);

		input.close();
	}

}