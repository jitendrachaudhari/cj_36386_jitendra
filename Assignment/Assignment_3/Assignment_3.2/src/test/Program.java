package test;

import java.util.Scanner;

class Employee{
	private String firstName;
	private String lastName;
	private double salary;
	
	Employee(){
		this("","",0.0);
	}
	
	public Employee(String firstName, String lastName ,double salary)
	{
		this.firstName=firstName;
		this.lastName=lastName;
		this.salary=salary;
		
		// if the monthly salary is not positive, set it to 0.0.
		 if (salary < 0.0)
		 {
		  salary = 0.0;
		 }
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
	
	public double getYearlySalary()
	{
		double yearlySalary = this.salary * 12;
		return yearlySalary;
	}
	
	// method to retrieve yearly salary after giving 10% raise
	public double getRaiseSalary()
	{
		double raise =  salary * 0.1 ;
		double raiseSalary = (( salary + raise ) * 12);
		return raiseSalary;
	} 
	
}

public class Program {
	
	public static void acceptRecord( Employee Emp)
	{
        Scanner sc = new Scanner(System.in);

		System.out.print("firstName	:	");
		Emp.setFirstName(sc.nextLine());
		
		System.out.print("lastName :  ");
		Emp.setLastName(sc.nextLine());
		
		
		System.out.print("salary:	");
		Emp.setSalary(sc.nextDouble());
		sc.close();
	}


	public static void main(String[] args) {
		Employee Emp = new Employee();
		Program.acceptRecord(Emp);
	    
	    
	    //double yearlySalary; 
	    System.out.println("Yearly Salary   :" +Emp.getYearlySalary());
	    //double raisedSalary;
	    System.out.println("Salary after raised  :" +Emp.getRaiseSalary());
	
	}

}