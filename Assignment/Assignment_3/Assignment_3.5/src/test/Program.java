package test;

import java.util.Scanner;

class CompoundInterest
{
	private int p;
	private int t;
	private double r;
	private int n;
	
	CompoundInterest(){
		this.p = 0;
		this.t = 0;
		this.r = 0.0;
		this.n = 0;
	}

	public CompoundInterest(int p, int t, double r, int n) {
		super();
		this.p = p;
		this.t = t;
		this.r = r;
		this.n = n;
	}

	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}
	
	public void calculate(int p, int t, double r, int n) {
        double amount = p * Math.pow(1 + (r / n), n * t);
        double cinterest = amount - p;
        System.out.println("Compound Interest after " + t + " years: "+cinterest);
        System.out.println("Amount after " + t + " years: "+amount);
    }
}

public class Program {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CompoundInterest CI = new CompoundInterest();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("ENTER THE P VALUE :     ");
		CI.setP(sc.nextInt());
		
		System.out.print("ENTER THE T VALUE :     ");
		CI.setT(sc.nextInt());
		
		System.out.print("ENTER THE R VALUE :     ");
		CI.setR(sc.nextDouble());
		
		System.out.print("ENTER THE N VALUE :     ");
		CI.setN(sc.nextInt());
		
		CI.calculate(2000, 2, 0.8, 10);
		
		
		
	}

}