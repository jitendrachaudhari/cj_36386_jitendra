package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

class Person {
	private String name;
	private Date birthdate;
	private String address;

	public Person() {
		this("",new Date(),"");
	}

	public Person(String name, Date birthdate, String address) {
		super();
		this.name = name;
		this.birthdate = birthdate;
		this.address = address;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj!=null)
		{
			Person other=(Person)obj;
			if(this.name==other.name)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "Person[name = "+name+",BirthDate= "+birthdate+ "Address= "+address+"]";
	}		
}

class SortByBirthdate implements Comparator<Person>{
	@Override
	public int compare(Person p1, Person p2) {
		//return p1.birthdate.compareTo(p2.birthdate) ;
		 return Long.valueOf(p1. getBirthdate().getTime()).compareTo(p2. getBirthdate().getTime());
	}
}

public class Program {
	private static void print(Person[] arr, Comparator<Person> c) {
		if( arr != null ) {
			Arrays.sort( arr, c );
			for( Person p : arr )
				System.out.println(p.toString());
			System.out.println();
		}
	}

	public static Person[] getPerson() throws ParseException {
		Person[] persons=new Person[4];
		persons[0]=new Person("Rubina", new SimpleDateFormat("dd-MM-yyyy").parse("23-11-1998"),"Solapur");
		persons[1]=new Person("Mahek",new SimpleDateFormat("dd-MM-yyyy").parse("11-10-2000"),"Pune");
		persons[2]=new Person("Sabiya",new SimpleDateFormat("dd-MM-yyyy").parse("20-12-2008"),"Karad");
		persons[3]=new Person("Archana",new SimpleDateFormat("dd-MM-yyyy").parse("18-11-1900"),"Karad");	

		return persons;
	}

	public static void main(String[] args) {
		Person[] arr;
		try {
			arr = Program.getPerson();
			Program.print(arr, new SortByBirthdate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
	}
}
