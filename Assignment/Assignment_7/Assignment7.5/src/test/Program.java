package test;

import java.util.Arrays;
import java.util.Comparator;

abstract class Player{
	private double income;

	public Player() {
	}

	public Player(double income) {
		this.income = income;
	}

	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	@Override
	public String toString() {
		return "Player [income=" + income + "]";
	}

}

class FootBallPlayer extends Player{
	private int matchCount;
	private int manOfMatchCount;

	public double getIncome() {	
		return this.manOfMatchCount*8000+this.matchCount*3000;
	}

	public FootBallPlayer(int matchCount, int manOfMatchCount) {
		super();
		this.matchCount = matchCount;
		this.manOfMatchCount = manOfMatchCount;
	}

	public int getMatchCount() {
		return matchCount;
	}

	public void setMatchCount(int matchCount) {
		this.matchCount = matchCount;
	}

	public int getManOfMatchCount() {
		return manOfMatchCount;
	}

	public void setManOfMatchCount(int manOfMatchCount) {
		this.manOfMatchCount = manOfMatchCount;
	}

	@Override
	public String toString() {
		return "FootBallPlayer [matchCount=" + matchCount + ", manOfMatchCount=" + manOfMatchCount + "]";
	}


}

class CricketPlayer extends Player{
	private int matchCount;
	private int manOfMatchCount;
	public CricketPlayer(int matchCount, int manOfMatchCount) {
		super();
		this.matchCount = matchCount;
		this.manOfMatchCount = manOfMatchCount;
	}
	public int getMatchCount() {
		return matchCount;
	}
	public void setMatchCount(int matchCount) {
		this.matchCount = matchCount;
	}
	public int getManOfMatchCount() {
		return manOfMatchCount;
	}
	public void setManOfMatchCount(int manOfMatchCount) {
		this.manOfMatchCount = manOfMatchCount;
	}
	public double getIncome() {
		return this.manOfMatchCount*20000+this.matchCount*10000;
	}
	@Override
	public String toString() {
		return "CricketPlayer [matchCount=" + matchCount + ", manOfMatchCount=" + manOfMatchCount + "]";
	}
	
}

class SortByIncome implements Comparator<Player>{
	@Override
	public int compare(Player p1, Player p2) {
		if(p1 instanceof CricketPlayer && p2 instanceof FootBallPlayer) {
			return (int) (p1.getIncome()-p2.getIncome());
		}else if(p1 instanceof FootBallPlayer && p2 instanceof FootBallPlayer)
		{
			return (int) (p1.getIncome()-p2.getIncome());
		}else if(p1 instanceof CricketPlayer && p2 instanceof FootBallPlayer) {
			return (int) (p1.getIncome()-p2.getIncome());
		}else {
			return (int) (p1.getIncome()-p2.getIncome());
		}
	}
	
}

public class Program{
	
	private static Player[] getPlayers() {
		Player[] arr=new Player[10];
		arr[0]=new CricketPlayer(2,0);
		arr[1]=new FootBallPlayer(2,0);
		arr[2]=new CricketPlayer(1,1);
		arr[3]=new CricketPlayer(1,2);
		arr[4]=new FootBallPlayer(0,1);
		arr[5]=new FootBallPlayer(2,2);
		arr[6]=new FootBallPlayer(2,4);
		arr[7]=new CricketPlayer(1,5);
		arr[8]=new CricketPlayer(1,0);
		arr[9]=new FootBallPlayer(2,3);
		return arr;
	}
	
	private static void print(Player[] p) {
		if(p != null) {
			for(Player p2 : p) {
				System.out.println(p2);

			}
		}
	}
	
	public static void main(String[] args) {
      Player[] player=Program.getPlayers();
      Program.print(player);
      System.out.println();
      Arrays.sort(player , new SortByIncome());
      Program.print(player);
	}
}