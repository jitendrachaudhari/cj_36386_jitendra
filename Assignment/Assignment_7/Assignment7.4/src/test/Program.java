package test;

import java.util.Arrays;
import java.util.Comparator;

abstract class Shape{
	private double area;
	private double peri;
	public Shape() {	}
	public Shape(int area, int peri) {
		this.area = area;
		this.peri = peri;
	}
	public abstract double getArea();
	public abstract double getPeri();
	@Override
	public String toString() {
		return "Shape [area=" + area + ", peri=" + peri + "]";
	}
	
}
class Circle extends Shape{
	private int radius;

	public Circle( int radius ) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public double getArea() {
		double area = Math.PI * Math.pow(radius, 2);
		return area;
	}

	@Override
	public double getPeri() {
		double peri = 2 * Math.PI* radius;
		return peri;
	}

	@Override
	public String toString() {
		return "Circle => Area=" + getArea()+ "Peri= " + getPeri()+ " ";
	}
	
	
}
class Rectangle extends Shape{
	private int length;
	private int breadth;
	public Rectangle(int length, int breadth) {
		this.length = length;
		this.breadth = breadth;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public int getBreadth() {
		return breadth;
	}
	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}
	@Override
	public double getArea() {
		
		return length*breadth;
	}
	@Override
	public double getPeri() {
		return 2*(length+breadth);
	}
	@Override
	public String toString() {
		super.toString();
		return "Rectangle => Area=" + getArea() + ", peri=" + getPeri() + " ," ;
	}
	
}
class SortByArea implements Comparator<Shape>{

	@Override
	public int compare(Shape s1, Shape s2) {
		if(s1 instanceof Circle && s2 instanceof Circle) {
			return (int) (s1.getArea() - s2.getArea());
		}else if(s1 instanceof Rectangle && s2 instanceof Rectangle) {
			return (int) (s1.getArea() - s2.getArea());
		}else if(s1 instanceof Circle && s2 instanceof Rectangle) {
			return (int) (s1.getArea() - s2.getArea());
		}else {
			return (int) (s1.getArea() - s2.getArea());
		}
	}
	
}
class SortByPeri implements Comparator<Shape>{

	@Override
	public int compare(Shape s1, Shape s2) {
		if(s1 instanceof Circle && s2 instanceof Circle) {
			return (int) (s1.getPeri() - s2.getPeri());
		}else if(s1 instanceof Rectangle && s2 instanceof Rectangle) {
			return (int) (s1.getPeri() - s2.getPeri());
		}else if(s1 instanceof Circle && s2 instanceof Rectangle) {
			return (int) (s1.getPeri() - s2.getPeri());
		}else {
			return (int) (s1.getPeri() - s2.getPeri());
		}
	}
	
}
public class Program {
	private static Shape[] getShape() {
		Shape[] s1 = new Shape[8];
		s1[0] = new Circle(5 );
		s1[1] = new Rectangle(10, 20);
		s1[2] = new Circle(100 );
		s1[3] = new Rectangle(100, 200);
		s1[4] = new Circle(15 );
		s1[5] = new Rectangle(45, 25);
		s1[6] = new Circle(45 );
		s1[7] = new Circle(26);
		return s1;

	}
	private static void print(Shape[] sh) {
		if(sh != null) {
			for(Shape s2 : sh) {
				System.out.println(s2);

			}
		}
	}
	public static void main(String[] args) {
		Shape[] sh = Program.getShape();
		Program.print(sh);
		System.out.println();
		System.out.println("Sort By Area: ");
		Arrays.sort(sh , new SortByArea());
		Program.print(sh);
		System.out.println();
		System.out.println("Sort By Perimeter");
		Arrays.sort(sh , new SortByPeri());
		Program.print(sh);
	}
}