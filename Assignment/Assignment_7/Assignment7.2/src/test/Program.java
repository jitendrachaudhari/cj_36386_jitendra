package test;

import java.util.Arrays;

class Address implements Comparable<Address>{
	private String city;
	private String state;
	private int pinCode;
	
	public Address(){
		this("","",0);
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public Address(String city, String state, int pinCode) {
		super();
		this.city = city;
		this.state = state;
		this.pinCode = pinCode;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj!=null)
		{
			Address other=(Address)obj;
			if(this.pinCode==other.pinCode)
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("%-15s%-15s%-5d", this.city, this.state, this.pinCode);
	}

	@Override
	public int compareTo(Address other) {
		return this.state.compareTo(other.state);
	}
	
	
}

public class Program {
	private static void print(Address[] arr) {
		if( arr != null ) {
			for( Address addr : arr )
				System.out.println(addr.toString());
			System.out.println();
		}
	}

	public static Address[] getAddress() {
		Address[] address=new Address[3];
		address[0]=new Address("Solapur","Maharashtra",413225);
		address[1]=new Address("Pune","Uttar Pradesh",413009);
		address[2]=new Address("Bijapur","Karnataka",413008);
		return address;
	}

	public static void main(String[] args) {
		Address[] arr=Program.getAddress();
		Program.print(arr);
		Arrays.sort(arr);
		Program.print(arr);
	}
}
