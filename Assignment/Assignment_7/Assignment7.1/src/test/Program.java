package test;

import java.util.Arrays;

class Date implements Comparable<Date>{
	private int day;
	private int month;
	private int year;

	Date(){
		this(0,0,0);
	}

	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public boolean equals(Object obj) {
		if( obj != null ) {
			Date other = (Date) obj;
			if( this.day==other.day && this.month==other.month && this.year == other.year )
				return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(this.day+" / "+this.month+" / "+this.year);
	}

	@Override
	public int compareTo(Date other) {
		return this.year-other.year;
	}

}

public class Program {
	private static void print(Date[] arr) {
		if( arr != null ) {
			for( Date date : arr )
				System.out.println(date.toString());
			System.out.println();
		}
	}

	public static Date[] getDates() {
		Date[] arr=new Date[5];
		arr[0]=new Date(23,11,1998);
		arr[1]=new Date(10,12,2004);
		arr[2]=new Date(13,10,2020);
		arr[3]=new Date(17,11,2021);
		arr[4]=new Date(19,12,1999);
		return arr;
	}

	public static void main(String[] args) {
		Date[] arr=Program.getDates();
		Program.print(arr);
		Arrays.sort(arr);
		Program.print(arr);
	}
}
