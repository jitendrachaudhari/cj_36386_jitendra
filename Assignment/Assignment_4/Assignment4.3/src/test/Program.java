package test;
import java.util.Scanner;

public class Program {
	private static Scanner in;

	public static void main(String[] args) {
		Company comp = new Company();
		in = new Scanner(System.in);
		System.out.println("Enter 0 to end");
		
		System.out.println("Enter the SalesPerson, Product and the Total Sales: ");
		comp.setSalesPerson(in.nextInt());
		if (comp.getSalesPerson() == 0)
		comp.setProduct(in.nextInt());
		comp.setValue(in.nextDouble());
		comp.setElements(comp.getSalesPerson() - 1, comp.getProduct() - 1, comp.getValue());
        
		comp.total(comp.getSales());
	}

}