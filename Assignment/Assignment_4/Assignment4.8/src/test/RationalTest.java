package test;

import java.util.Scanner;

public class RationalTest 
{
	public static void main(String[] args) 
	{

		Scanner input = new Scanner(System.in);
		Rational a;
		Rational b;

		int numerator_a,denominator_a,numerator_b,denominator_b;

		System.out.println("Enter numerator for the first rational number: ");
		numerator_a = input.nextInt();
		System.out.println("Enter a denominator for the first rational number: ");
		denominator_a = input.nextInt();
		System.out.println("Enter a numerator for the second rational number: ");
		numerator_b = input.nextInt();
		System.out.println("Enter a denominator for the second rational number: ");
		denominator_b = input.nextInt();

		a = new Rational(numerator_a, denominator_a);
		b = new Rational(numerator_b, denominator_b);

		System.out.println("First rational number is: " + a);
		System.out.println("Second rational number is: " + b);
		System.out.println("Addition of the rational number is: " + a.addition(b));
		System.out.println("Subtraction of the rational number is: " + a.subtraction(b));
		System.out.println("Multiplication of the rational number is: " + a.multiplication(b));
		System.out.println("Division of the rational number is: " + a.division(b));

		input.close();

	}
}