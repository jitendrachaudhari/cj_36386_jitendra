package test;

public class Rational 
{

	private int numerator;
	private int denominator;

	public Rational() {
		numerator = 1;
		denominator = 2;
	}

	public Rational(int numerator, int denominator) {

		this.numerator = numerator;
		this.denominator = denominator;
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}

	public Rational addition(Rational r) {
		int num = numerator * r.denominator + r.numerator * denominator;
		int denom = denominator * r.denominator;
		return new Rational(num, denom);
	}

	public Rational subtraction(Rational r) {
		int num = numerator * r.denominator - r.numerator * denominator;
		int denom = denominator * r.denominator;
		return new Rational(num, denom);
	}

	public Rational multiplication(Rational r) {
		int num = numerator * r.numerator;
		int denom = denominator * r.denominator;
		return new Rational(num, denom);
	}

	public Rational division(Rational r) {
		int num = numerator / r.numerator;
		int denom = denominator / r.denominator;
		return new Rational(num, denom);
	}

	@Override
	public String toString() {
		return "Rational [numerator=" + numerator + ", denominator=" + denominator + "]";
	}

}