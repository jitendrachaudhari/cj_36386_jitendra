package test;

public class SavingsAccount {
	private static int annualInterestRate;
	private  double savingsBalance;
	
	public static int getAnnualInterestRate() {
		return annualInterestRate;
	}
	public static void setAnnualInterestRate(int annualInterestRate) {
		SavingsAccount.annualInterestRate = annualInterestRate;
	}
	public  double getSavingsBalance() {
		return savingsBalance;
	}
	public SavingsAccount(double savingsBalance) {
		super();
		this.savingsBalance = savingsBalance;
	}
	public  void setSavingsBalance(double savingsBalance) {
		this.savingsBalance = savingsBalance;
	}
	
	public  double calculateMonthlyInterest()
	{
		//double monthlyInterest;
		 return (this.savingsBalance*annualInterestRate)/12;
	}
	public static void  modifyInterestRate(int annualInterestRate) {
		SavingsAccount.annualInterestRate=annualInterestRate;
	}
	public void DisplayRecord()
	{
		System.out.println("Balance :  "+this.savingsBalance);
		System.out.println("Monthly Interest  :  " +   this.calculateMonthlyInterest());
	}
}
