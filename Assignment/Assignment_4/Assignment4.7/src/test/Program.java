package test;

public class Program{
	public static void main(String[] args) {
		SavingsAccount saver1 =new SavingsAccount(2000);
		SavingsAccount saver2 =new SavingsAccount(3000);
		//SavingsAccount.setSavingsBalance(2000);
		//saver1.setSavingsBalance(3000);
		SavingsAccount.setAnnualInterestRate(4);
		saver1.DisplayRecord();
		saver2.DisplayRecord();
		SavingsAccount.setAnnualInterestRate(5);
		System.out.println("***************new Annual Interest***************");
				saver1.DisplayRecord();
				saver2.DisplayRecord();
	}
}