package test;

import java.util.Arrays;

public class Program
{
	private static void printArray(int[][] arr) {
		if( arr != null ) {
			for( int row = 0; row < arr.length; ++ row ) {
				for( int col = 0; col < arr[ row ].length; ++ col ) {
					System.out.print(arr[ row ][ col ]+"	");
				}
				System.out.println();
			}
		}
	}
	public static void main(String[] args)
	{
		int[][] arr=new int[4][];
		arr[0]=new int [] {4,3,2,1};
		arr[1]=new int [] {3,2,1};
		arr[2]=new int [] {2,1};
		arr[3]=new int [] {1};
		Program.printArray(arr);		
	}
}
