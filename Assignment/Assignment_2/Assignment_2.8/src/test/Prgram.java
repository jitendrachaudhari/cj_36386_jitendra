package test;
import java.util.Scanner;
public class Prgram {
	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
        
        double currentPopulation; 
        double growthRate;
        double annualPopIncrease;
        double estimatedPopulation;
         
        System.out.print ("Enter Current World Population: ");
        currentPopulation = sc.nextLong();
        
        System.out.print ("Enter Annual Population Growth Rate: ");
        growthRate = sc.nextInt();
        
        annualPopIncrease = (growthRate / 100) * currentPopulation;
        
        estimatedPopulation = currentPopulation + annualPopIncrease;
        
        System.out.println("Estimated population after one year:     " + estimatedPopulation);
        System.out.println("Estimated population after two years:    " + estimatedPopulation + (annualPopIncrease * 2));
        System.out.println("Estimated population after three years:  " + estimatedPopulation + (annualPopIncrease * 3));
        System.out.println("Estimated population after four years:   " + estimatedPopulation + (annualPopIncrease * 4));
        System.out.println("Estimated population after five years:   " + estimatedPopulation + (annualPopIncrease * 5));
	}
}